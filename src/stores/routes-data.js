import { computed, reactive, ref } from 'vue'
import { defineStore } from 'pinia'
import L from 'leaflet'
import ForwardMarker from '@/assets/forward-marker.svg'
import BackwardMarker from '@/assets/backward-marker.svg'
import RoutesService from '@/services/routes.service.js'
import { useRouter } from 'vue-router'

export const useRoutesDataStore = defineStore('routes-data', () => {

    const router = useRouter();

    const renderTypes = {
      routes: 'routes',
      stops: 'stops'
    }

    const state = reactive({
        renderType: renderTypes.routes,
        loading: true,
        routes: [],
        currentRouteToCenter: null,
        currentStopToCenter: null
    });

    const routesPolyline = computed(() => state.routes.reduce((prev, current) => {
        const polyline = L.polyline(prepareRoutePoints(current.Points), {color: '#e1660a'});
        polyline.id = current.ID;
        polyline.bindTooltip(current.Name, {
          sticky: true
        });
        polyline.on('click', function(e) {
          state.currentRouteToCenter = current.ID;
          router.push(`/routes/${current.ID}`);
        });

        return {
            ...prev,
            [current.ID]: polyline
        }
    }, {}));

    const routesStopMarkersLayers = computed(() => state.routes.reduce((prev, current) => {
      return {
        ...prev,
        [current.ID]: prepareRouteStopMarkersLayers(current.Stops)
      }
    }, {}));

    const routeById = (routeId) => {
      return computed(() => state.routes.find((route) => route.ID == routeId));
    }

    const prepareRoutePoints = (points) => {
      if (!points) return [];
      return points.map(point => [point.Lat, point.Lon]);
    }

    const prepareRouteStopMarkersLayers = (stops) => {
      const markersLayer = L.layerGroup()
      stops.forEach(stop => {
        const stopMarker = L.marker([stop.Lat, stop.Lon], { icon: L.icon({
            iconUrl: stop.Forward ? ForwardMarker : BackwardMarker,
            iconAnchor: [10, 10],
          })}).addTo(markersLayer);
        stopMarker.id = stop.ID;
        stopMarker.bindTooltip(stop.Name);
      });
      return markersLayer;
    }

    const getRoutes =  () => {
        RoutesService.getRoutes()
          .then((payload) => state.routes = payload)
          .finally(() => state.loading = false)
    }

    const setRenderType = (type) => {
        state.renderType = type;
    }

    return {
        state,
        renderTypes,
        getRoutes,
        routeById,
        setRenderType,
        routesPolyline,
        routesStopMarkersLayers
    }
})