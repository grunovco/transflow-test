import {$api} from '@/http/index.js';
import routesData from '../routes'

export default class RoutesService {
    static getRoutes() {
        // const response = await $api.get('/routes_data?key=012345678abc');
        // return response.data;
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(routesData)
            }, 1000)
        })
    }
}