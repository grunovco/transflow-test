import axios from 'axios'

export const $api = axios.create({
    baseURL: `https://220.transflow.ru/api/public/v1`
})