import {createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import RoutesView from '@/views/RoutesView.vue'
import StopsView from '@/views/StopsView.vue'
import RouteView from '@/views/RouteView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      redirect: '/routes',
      children: [
        {
          path: '/routes',
          name: 'routes',
          component: RoutesView
        },
        {
          path: '/stops',
          name: 'stops',
          component: StopsView
        },
        {
          path: '/routes/:id',
          name: 'routeById',
          component: RouteView
        }
      ]
    }
  ]
})

export default router
